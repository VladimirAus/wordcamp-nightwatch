<< Back to main [README](../README.md)

# Docker image setup for WordPress

## Content

Image is available on [GitLab Registry](https://gitlab.com/VladimirAus/wordcamp-nightwatch/container_registry).

Based on [php7 with apache](https://hub.docker.com/_/php/) official image.

Includes:

* PHP 7.0.20 and Apache 2.4 (Does not include database, )
* [Composer](https://getcomposer.org/)
* [WP CLI tool](http://wp-cli.org/)
* [NodeJS](https://nodejs.org/) v8.1.3 and [Yarn](https://yarnpkg.com/) v0.27.5

## Build

To build new image locally make sure you have [docker](https://www.docker.com/) installed.

```
docker build -t registry.gitlab.com/vladimiraus/wordcamp-nightwatch ./php-apache
```

To run single container use

```

```

NOTE. If you are looking to run the full setup, use notes from main [README](../README.md) file.

<< Back to main [README](../README.md)
