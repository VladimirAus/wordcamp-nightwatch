WORDCAMP_PROJECT_CONTAINER='docker exec -it wordcamp-web'
WORDCAMP_DOCROOT='/var/app/web'
# Composer commands
function wordcamp-composer () {
    export COMPOSER_PROCESS_TIMEOUT=600
    time  ${WORDCAMP_PROJECT_CONTAINER} composer "$@" -vvv --profile
    # time  ${WORDCAMP_PROJECT_CONTAINER} bash -c `export COMPOSER_PROCESS_TIMEOUT=600 && composer "$@" -vvv --profile`
}
# WP commands
function wordcamp-wp () {
    time ${WORDCAMP_PROJECT_CONTAINER} wp --allow-root --path=/var/app/web "$@"
}
# Allow custom commands against the container
function wordcamp-run () {
    time ${WORDCAMP_PROJECT_CONTAINER} "$@"
}