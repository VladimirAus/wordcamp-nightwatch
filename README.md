# Wordpress automated testing example using NightwatchJS

Repository for [Testing any day: guide to end to end test driven WordPress projects](https://2017.brisbane.wordcamp.org/session/testing-any-day-guide-to-end-to-end-test-driven-wordpress-projects/) session presented at [WordCamp Brisbane](https://2017.brisbane.wordcamp.org/). 

## Run tests

* [Locally using Docker](#tesing-locally)
* [In GitLab CI environment](#tesing-on-gitlab)

### Tesing locally

Follow these steps:

1. Prerequisites

  * Install [Docker](https://www.docker.com/) & `Docker Compose` 
  (usually included in Docker for Mac and Win).

2. Create folders

```
mkdir logs
mkdir web
```

3. Build containers

```
docker-compose -f docker-compose.nightwatch.yml up -d --build
```

NOTE. If you are looking to run the full setup, use notes from main [README](./docker/README.md) file.

4. Load helper container functions

```
. bin/dot.bashrc
```

Adds the following commands 

* `wordcamp-wp`: runs `wp` command from website folder 
* `wordcamp-run`: executes any command in project folder (not wordpress folder)
* `wordcamp-composer`: executes `composer` command

5. Download and install Wordpress

```
wordcamp-wp core download --locale=en_AU
wordcamp-wp core config --dbname=wordcamp_wordpress --dbuser=wordcamp2017user \
            --dbpass=sky3pwd --dbhost=wordcamp-mysql
wordcamp-wp core install --url="127.0.0.1" --title="WordCamp Brisbane" \ 
            --admin_user=wcuser --admin_password=strongpassword --admin_email=info@example.com
wordcamp-wp user create bob bob@example.com --role=author --user_pass=bobpass
```

6. Initiate (download dependencies) and run tests

```
wordcamp-run bash -c "cd nightwatch && yarn install"
wordcamp-run bash -c "cd nightwatch && . bin/dotenv.local && yarn run test-anonymous"
```

### Tesing on GitLab

See [.gitlab-ci.yml](./.gitlab-ci.yml) file for steps. See results [here](https://gitlab.com/VladimirAus/wordcamp-nightwatch/pipelines)

## Other services

Checkout the following services that

* Already have selenium setup
* Have UI to setup testing on different browser version for desktop and mobile

Services

* [SauseLabs](https://saucelabs.com/)
* [BrowserStack](https://www.browserstack.com/)

Links

* [NightwatchJS](http://nightwatchjs.org/)
* [Docker](https://www.docker.com/)
* [GitLab CI](https://about.gitlab.com/features/gitlab-ci-cd/)

## Read more

* [Docker setup](./docker/README.md)
* [Nightwatch setup](./nightwatch/README.md)
