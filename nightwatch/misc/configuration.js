// configuration.js

// TEST_URL check.
if ((process.env.TEST_URL === '') || (process.env.TEST_URL.indexOf('http') >= 0)) {
  var message = new Error('Environmental variable TEST_URL either empty or includes protocol.');
  process.emitWarning(message, 'Warning');
  process.exitCode = 1;
}

var url = 'http://' + process.env.TEST_URL + '/';
var defaultTimeout = 1000;
var defaultPageLoad = defaultTimeout * 3;
var defaultLongWait = defaultTimeout * 15;

// Export it.
exports.url = url;
exports.timeout = defaultTimeout;
exports.pageLoad = defaultPageLoad;
exports.longWait = defaultLongWait;
