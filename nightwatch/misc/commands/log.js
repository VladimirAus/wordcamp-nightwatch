exports.command = function (message, callback) {
  'use strict';
  return this.perform(function (browser, done) {
    /* eslint-disable */
    console.log('\x1B[34m ¡ \x1B[0m' + message);
    /* eslint-enable */
    done();
  });
};
