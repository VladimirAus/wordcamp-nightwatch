var util = require('util');
var events = require('events');
var conf = require('../configuration');

function WpLoginAs() {
  'use strict';
  events.EventEmitter.call(this);
}

util.inherits(WpLoginAs, events.EventEmitter);

WpLoginAs.prototype.command = function (username, password, cb) {
  'use strict';
  var self = this;

  self.client.api
    .log(' -WORDPRESS- Login: start')
    .url(conf.url + 'wp-login.php?loggedout=true')
    .waitForElementVisible('input#user_login', conf.timeout, true, function (result) {

      self.client.api
        .log(' -WORDPRESS- Login: logging in as ' + username)
        .setValue('input#user_login', username)
        .setValue('input#user_pass', password)
        .click('input#wp-submit', function () {
          // if we have a callback, call it right before the complete event
          if (cb) {
            cb.call(self.client.api);
          }

          self.emit('complete');
        });
    });

  return this;
};

module.exports = WpLoginAs;
