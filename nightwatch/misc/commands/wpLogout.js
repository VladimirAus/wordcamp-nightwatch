var util = require('util');
var events = require('events');
var conf = require('../configuration');

function WpLogout() {
  'use strict';
  events.EventEmitter.call(this);
}

util.inherits(WpLogout, events.EventEmitter);

WpLogout.prototype.command = function (cb) {
  'use strict';
  var self = this;

  self.client.api
    .log(' -WORDPRESS- Logout: start')
    .url(conf.url + 'wp-login.php?loggedout=true', function (result) {

      self.client.api.url(function (response) {
        self.client.api.log('Current URL: ' + response.value); // outputs the current url
      });
      self.client.api.getText('body', function (result) {
        self.client.api.log(result.value);
      });

      self.client.api.waitForElementVisible('body.login', conf.pageLoad, true, function (result) {
        self.client.api.containsText('.message', 'You are now logged out.', function (result) {
          // if we have a callback, call it right before the complete event
          if (cb) {
            cb.call(self.client.api);
          }

          self.client.api.log(' -WORDPRESS- Logout: successful');
          self.emit('complete');
        });
      });
    });

  return this;
};

module.exports = WpLogout;
