<< Back to main [README](../README.md)

# Test suite

Test suite based on [Selenium](http://www.seleniumhq.org/) and [NightwatchJS](http://nightwatchjs.org/).

## Structure

* `tests` - location of the actual tests. 
* `nightwatch.json` - nightwatch configuration. 
* `package.json` - configuration file for `node` or `yarn`
* `misc` - location of custom commands.
* `bin` - scripts and setup files
* `reports` - outcome of the tests

## Custom commands

* location defined in `nightwatch.json`
* located in `misc/commands` folder

### List of commands
* `log(message)` - outputs info message. Safer and eslint friendly way to output information.
* `wpLoginAs(username, password)` - attempts to log Wordpress user in.
* `wpLogout()` - attempts to log Wordpress user out.

To define your own commands go to [Nightwatch help](http://nightwatchjs.org/guide#writing-custom-commands).

<< Back to main [README](../README.md)
