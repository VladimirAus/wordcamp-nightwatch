var conf = require('../misc/configuration');

module.exports = {
  '@tags': [
    'anonymous'
  ],
  'beforeEach': function (browser) {
    'use strict';

    browser.url(function (response) {
      // Outputs the current url.
      browser.log('Current URL: ' + response.value);
    });
  },
  'As an anonymous user I want to see the front page with WORDCAMP BRISBANE title.': function (browser) {
    'use strict';

    var elemPage = 'h1.site-title';
    var valueWebsiteTitle = 'WORDCAMP BRISBANE';

    browser
      // Logout and load home page.
      .url(conf.url);

    // Output current URL.
    browser.url(function (response) {
      browser.log('Current URL: ' + response.value); // outputs the current url
    });
    // Output page content.
    browser.getText('body', function (result) {
      browser.log(result.value);
    });

      // Check that title is visible.
    browser
      .waitForElementVisible(elemPage, conf.pageLoad)
      .verify.containsText(elemPage, valueWebsiteTitle);
  }
};
