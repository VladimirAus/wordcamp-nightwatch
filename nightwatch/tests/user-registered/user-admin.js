var conf = require('../../misc/configuration');

module.exports = {
  '@tags': [
    'registered', 'admin'
  ],
  'beforeEach': function (browser) {
    'use strict';

    browser.url(function (response) {
      // Outputs the current url.
      browser.log('Current URL: ' + response.value);
    });
  },
  'As an administrator I want to login and access see settings menu item.': function (browser) {
    'use strict';

    var elemMenuAdmin = '#adminmenu';
    var valueMenuAdminSettings = 'Settings';

    browser
      // Logout and load home page.
      .wpLoginAs('wcuser', 'wrongpassword')
      .waitForElementVisible('body', conf.pageLoad)
      .verify.containsText('body', 'The password you entered for the username wcuser is incorrect.')
      .wpLoginAs('wcuser', 'strongpassword');

    // Check that menu is visible and has Settings menu item.
    browser
      .waitForElementVisible(elemMenuAdmin, conf.pageLoad)
      .verify.containsText(elemMenuAdmin, valueMenuAdminSettings);
  }
};
