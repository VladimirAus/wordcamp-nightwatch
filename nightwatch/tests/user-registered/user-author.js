var conf = require('../../misc/configuration');

module.exports = {
  '@tags': [
    'registered', 'author'
  ],
  'beforeEach': function (browser) {
    'use strict';

    browser.url(function (response) {
      // Outputs the current url.
      browser.log('Current URL: ' + response.value);
    });
  },
  'As an administrator I want to login and access see settings menu item.': function (browser) {
    'use strict';

    var elemMenuAdmin = '#adminmenu';
    var valueMenuAdminSettings = 'Settings';
    var valueMenuAdminTools = 'Tools';

    browser
    // Logout and load home page.
      .wpLoginAs('bob', 'bobpass');

    // Check that admin menu is visible and has Tools menu item.
    browser
      .waitForElementVisible(elemMenuAdmin, conf.pageLoad)
      .verify.containsText(elemMenuAdmin, valueMenuAdminTools);

    // Check that admin menu is visible but Settings menu item is not.
    browser.expect.element(elemMenuAdmin).text.to.not.contain(valueMenuAdminSettings);
  }
};
